webpath="/var/www/${1}"
echo "Creating web service for $1 in ${webpath}"
if [ -d $webpath ]; then
	echo "Web service $1 already exists! Aborting..."
	exit 0
fi
mkdir $webpath
mkdir "$webpath/logs"
mkdir "$webpath/socks"
mkdir "$webpath/public"
mkdir "$webpath/exports"

# NEED TO ADD CRON FOR POSTGRESQL BACKUP

cat >/etc/nginx/sites-available/${1} <<EOF
server {
	listen 80;
	listen [::]:80;

	root /var/www/${1}/public;
	error_log /var/www/${1}/logs/error.log;

	index index.php index.html index.htm index.nginx-debian.html;

	server_name ${1};

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ =404;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
	
		fastcgi_pass unix:/var/www/${1}/socks/php.sock;
	}
}
EOF

cat >/etc/php/7.0/fpm/pool.d/${1}.conf <<EOF
[${1}]
user = www-data
group = www-data
listen = /var/www/${1}/socks/php.sock

listen.owner = www-data
listen.group = www-data
listen.allowed_clients = 127.0.0.1

pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 2
pm.max_spare_servers = 3
pm.max_requests = 500
slowlog = /var/www/${1}/logs/php.slow.log
request_slowlog_timeout = 2s
catch_workers_output = yes

php_flag[display_errors] = on
php_flag[display_startup_errors] = on
php_admin_value[error_log] = /var/www/${1}/logs/php.log
php_admin_flag[log_errors] = on
EOF


# Enable everything
ln -s "/etc/nginx/sites-available/${1}" "/etc/nginx/sites-enabled/${1}"
nginx -t && systemctl restart nginx
systemctl restart php7.0-fpm

# Generate random password for PostgreSQL access
webpassword=$(openssl rand -base64 32)
echo "This is your PostgreSQL password for ${1}: ${webpassword}" > "$webpath/README"

# Create PostgreSQL user and db
#psql -U admin -c "CREATE USER $1 WITH PASSWORD ``"
#createdb -O $1 $1
